//
//  GenerateStory.h
//  SagoApp
//
//  Created by Anders Zetterström on 2015-01-29.
//  Copyright (c) 2015 Anders Zetterström. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GenerateStory : NSObject

-(NSString*) setStory;
@end
