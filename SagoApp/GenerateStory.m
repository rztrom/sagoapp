//
//  GenerateStory.m
//  SagoApp
//
//  Created by Anders Zetterström on 2015-01-29.
//  Copyright (c) 2015 Anders Zetterström. All rights reserved.
//

#import "GenerateStory.h"

@interface GenerateStory ()
@property NSArray *nouns;
@property NSArray *adjectives;
@property NSArray *greetings;
@property NSArray *places;
@property NSArray *characters;
@property NSArray *verb;
@property NSArray *happenings;
@property NSArray *listOfWords;

@end

@implementation GenerateStory

-(void) setArrays{
    self.nouns=@[@"smycke", @"garnnystan", @"bildäck", @"implantat", @"halsband", @"smycke"];
    self.adjectives=@[@"skön", @"nice", @"snygg", @"hal", @"mörk", @"smal", @"oval", @"blodig", @"ljus", @"otäck", @"fasansfull", @"levande", @"hård"];
    self.greetings=@[@"Hej där!", @"Goddag!",@"Tjena!",@"Skepp och Hoj!", @"Du ska dö!", @"Mohahahhaha!", @"Mjauu"];
    self.places=@[@"skogsdungen", @"källaren", @"skorstenen", @"graven", @"bäcken", @"bilen" ];
    self.characters=@[@"gubbe", @"hamster", @"flicka", @"superhjälte", @"daggmask", @"docka", @"pannkaka", @"försökskanin"];
    self.verb=@[@"hoppa", @"hugga", @"äta", @"mörda", @"röka på", @"tälja tandpetare", @"blanda cocktails", @"smyga runt"];
    self.happenings=@[@"översvämning", @"fin dag", @"snöstorm", @"värmebölja"];

}

// retunerar ett random ord från en array
-(NSString*) randomWord:(NSArray*)array{
    return array[arc4random()%array.count];
}

-(NSArray*) setRandomWords {
self.listOfWords=@[[self randomWord:self.adjectives],[self randomWord:self.nouns],[self randomWord:self.verb],[self randomWord:self.happenings],[self randomWord:self.greetings],[self randomWord:self.places],[self randomWord:self.characters]];
    
    // 0. adj
    // 1. sub
    // 2. verb
    // 3. händlse
    // 4. hälsning
    // 5. ställe
    // 6. karaktär
    
    return self.listOfWords;
}


-(NSString*) setPartOne {
    // sätter alla Arrayer
    [self setArrays];
    [self setRandomWords];
 
      NSString *partOne= [NSString stringWithFormat:@"Det var en gång en %@ %@ som hade ett litet %@. Ibland är det inte dumt att få vara en %@. En gång gick denna till %@ för att %@ lite. Dagen till ära var det en %@.",
                          self.listOfWords[0],    // 1. adjektiv
                          self.listOfWords[6],    // 2. karaktär
                          self.listOfWords[1],    // 3. substantiv
                          self.listOfWords[6],     //4. karaktär
                          self.listOfWords[5],    // 5. ställe
                          self.listOfWords[2],   // 6. verb
                          self.listOfWords[3]];     //7. händelse
    return partOne;
}

-(NSString*) setPartTwo{
    
    [self setRandomWords];
    NSString *partTwo= [NSString stringWithFormat:@"Väl framme så dök plötsligt en %@ upp. De möttes och en av dom sa -%@!. Så mycket mer utbyttes inte innan dom började %@  i %@ som var så %@, vilket uppskattades mycket. Att %@ är det bästa dom vet. Snipp snapp snut, så var sagan slut!",
                        self.listOfWords[6],    //.1 karaktär
                        self.listOfWords[4],    // 2. hälsning
                        self.listOfWords[2],    // 3. verb
                        self.listOfWords[5],    // 4. ställe
                        self.listOfWords[0],    // 5. adjektiv
                        self.listOfWords[2]];     // 6. verb
    return partTwo;
}



-(NSString*) setStory{
   
    NSString *fullStory=[NSString stringWithFormat:@"%@ %@",[self setPartOne],[self setPartTwo]];
    return fullStory;
}
@end
