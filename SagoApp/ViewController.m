//
//  ViewController.m
//  SagoApp
//
//  Created by Anders Zetterström on 2015-01-26.
//  Copyright (c) 2015 Anders Zetterström. All rights reserved.
//

#import "ViewController.h"
#import "GenerateStory.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITextView *storyText;

@end

@implementation ViewController

- (IBAction)newStory:(id)sender {
    GenerateStory *gs = [[GenerateStory alloc] init];
    self.storyText.text= [gs setStory];
}


- (void)viewDidLoad {
    [super viewDidLoad];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
   
}

@end
